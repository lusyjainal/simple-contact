import {object, string, number} from 'yup';

export default object({
  firstName: string().required('First Name is required'),
  lastName:  string().required('Last Name is required'),
  age: number().integer().min(1, 'Minimum 1').required('Age is required'),
  photo: string().required('Link Photo is required'),
});
