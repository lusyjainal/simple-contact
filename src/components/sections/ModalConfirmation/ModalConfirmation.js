import React, {useState} from 'react';
import {Alert, Modal, StyleSheet, Text, Pressable, View} from 'react-native';
import TouchableScale from 'react-native-touchable-scale';

const ModalConfirmation = ({
  isLoadingConfirm,
  fullName,
  modalVisible,
  setModalVisible,
  handleYes,
  handleCancel,
}) => {
  //   const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              Are you sure delete{' '}
              <Text style={{fontWeight: 'bold'}}>{fullName}</Text>
            </Text>
            <View style={styles.wrapButton}>
              <TouchableScale
                style={[styles.button, styles.buttonClose]}
                onPress={handleCancel}>
                <Text style={styles.textStyle}>Cancel</Text>
              </TouchableScale>
              <TouchableScale
                style={[styles.button, styles.buttonYes]}
                onPress={handleYes}>
                <Text style={styles.textStyle}>{isLoadingConfirm ? 'loading...' : 'Yes'}</Text>
              </TouchableScale>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapButton: {
    flexDirection: 'row',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: 'red',
    marginRight: 10,
  },
  buttonYes: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default ModalConfirmation;
