import WrapListContainer from './WrapListContainer';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainActions from '../../../redux/contact/action';

const mapStateToProps = state => {
    return {
        contactData: state.contact
    }
}

const mapDispatchToProps = dispatch => {
    return {
        mainActions: bindActionCreators(mainActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrapListContainer)