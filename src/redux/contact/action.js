import { ACTIONS } from "../../constants";

export function getContactDetail(contact) {
    return {
        type: ACTIONS.GET_CONTACT_DETAIL,
        contact
    }
}