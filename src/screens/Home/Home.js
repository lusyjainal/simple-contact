/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Dimensions, View} from 'react-native';
import {WrapList} from '../../components/sections';
import {Icon, Text, Card, Button } from '@rneui/themed';
import TouchableScale from 'react-native-touchable-scale';

const Home = () => {
  return (
    <SafeAreaView>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.wrapScroolView}>
        <WrapList />
      </ScrollView>
      {/* <View style={[styles.wrapButtonAdd, styles.elevation]}>
        <TouchableScale style={styles.btn}>
          <Icon reverse name="add" type="ionicon" color="#517fa4" />
          <Text style={styles.text}>Add Contact</Text>
        </TouchableScale>
      </View> */}
    </SafeAreaView>
  );
};

let height = Dimensions.get('window').height; //full height

const styles = StyleSheet.create({
  wrapSafeArea: {
    // position: 'absolute'
  },  
  wrapScroolView: {
    // paddingTop: 10,
    padding: 10,
    height: height
  },
  wrapButtonAdd: {
    right: 30,
    left: 30,
    position: 'absolute',
    bottom: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    height: 60
  },
  shadowProp: {
    shadowColor: '#171717',
    shadowOffset: {width: -2, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  elevation: {
    elevation: 100,
    shadowColor: '#52006A',
  },
  btn: {
    marginTop: -30
  },
  text: {
    fontWeight: 'bold',
    color: 'black',
    fontStyle: 'italic'
  }
});

export default Home;
