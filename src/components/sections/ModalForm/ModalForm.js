import React, {useState} from 'react';
import {Alert, Modal, StyleSheet, Text, Pressable, View} from 'react-native';
import TouchableScale from 'react-native-touchable-scale';
import {Icon, Input} from '@rneui/themed';
import {Formik} from 'formik';
import validation from './validation';

const ModalForm = ({
  contact,
  handleSubmitData,
  isLoading,
  modalVisible,
  setModalVisible,
  handleCancel,
}) => {
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <Formik
            validationSchema={validation}
            initialValues={{
              firstName: contact.firstName || '',
              lastName: contact.lastName || '',
              age: contact.age ? contact.age.toString() : null,
              photo: contact.photo || '',
            }}
            onSubmit={handleSubmitData}>
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              isValid,
              dirty,
              touched,
            }) => (
              <View style={styles.modalView}>
                <Input
                  placeholder="First Name"
                  onChangeText={handleChange('firstName')}
                  onBlur={handleBlur('firstName')}
                  value={values.firstName}
                  errorStyle={{color: 'red'}}
                  errorMessage={errors.firstName && touched.firstName && `${errors.firstName}`}
                />
                <Input
                  placeholder="Last Name"
                  onChangeText={handleChange('lastName')}
                  onBlur={handleBlur('lastName')}
                  value={values.lastName}
                  errorStyle={{color: 'red'}}
                  errorMessage={errors.lastName && touched.lastName && `${errors.lastName}`}
                />
                <Input
                  placeholder="Age"
                  onChangeText={handleChange('age')}
                  onBlur={handleBlur('age')}
                  value={values.age}
                  keyboardType="numeric"
                  errorStyle={{color: 'red'}}
                  errorMessage={errors.age && touched.age && `${errors.age}`}
                />
                <Input
                  placeholder="Url Photo"
                  onChangeText={handleChange('photo')}
                  onBlur={handleBlur('photo')}
                  value={values.photo}
                  errorStyle={{color: 'red'}}
                  errorMessage={errors.photo && touched.photo && `${errors.photo}`}
                />
                <View style={styles.wrapButton}>
                  <TouchableScale
                    style={[styles.button, styles.buttonClose]}
                    onPress={handleCancel}>
                    <Text style={styles.textStyle}>Cancel</Text>
                  </TouchableScale>
                  <TouchableScale
                    style={
                      isValid == true && dirty == true
                        ? [styles.button, styles.buttonSave]
                        : [styles.button, styles.disabled]
                    }
                    onPress={handleSubmit}
                    disabled={!isValid}>
                    <Text style={styles.textStyle}>
                      {isLoading ? 'Loading...' : 'Save'}
                    </Text>
                  </TouchableScale>
                </View>
              </View>
            )}
          </Formik>
        </View>
      </Modal>
      <TouchableScale style={styles.btn} onPress={() => setModalVisible(true)}>
        <Icon reverse name="add" type="ionicon" color="#517fa4" />
      </TouchableScale>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapButton: {
    flexDirection: 'row',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    marginBottom: 22,
  },
  modalView: {
    width: 300,
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 100,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonDisbled: {
    backgroundColor: 'grey',
  },
  buttonClose: {
    marginRight: 10,
    backgroundColor: 'red',
  },
  buttonSave: {
    backgroundColor: 'blue',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  errorText: {
    color: 'red'
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default ModalForm;
