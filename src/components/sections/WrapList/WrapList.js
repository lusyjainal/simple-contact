import React, {useState, useEffect} from 'react';
import {Alert, View, ActivityIndicator} from 'react-native';
import {ListElement} from '../../UI';
import ModalConfirmation from '../ModalConfirmation';
import ModalForm from '../ModalForm';



/**
 * @description
 * file container di gunakan untuk kebutuhan view
 */

export default function WrapList({
    contact,
    isLoading,
    isLoadingForm,
    isLoadingConfirm,
    fullName,
    modalVisibleConfirm,
    setModalVisibleConfirm,
    modalVisibleForm,
    setModalVisibleForm,
    handleSubmitData,
    showModalForm,
    showModal,
    handleCancel,
    handleDelete
}) {

  return (
    <View>
      <ModalForm
        handleSubmitData={handleSubmitData}
        isLoading={isLoadingForm}
        modalVisible={modalVisibleForm}
        setModalVisible={setModalVisibleForm}
        handleCancel={handleCancel}
      />
      {(() => {
        if (isLoading)
          return <ActivityIndicator size="large" color="#00ff00" />;
        if (contact)
          return contact?.map((list, idx) => (
            <ListElement
              id={list.id}
              fullName={`${list.firstName} ${list.lastName}`}
              age={list.age}
              photo={list.photo}
              modulus={idx % 2}
              key={idx}
              handleDelete={showModal}
              handleEdit={()=>showModalForm(list)}
            />
          ));
      })()}
      <ModalConfirmation
        fullName={fullName}
        isLoadingConfirm={isLoadingConfirm}
        modalVisible={modalVisibleConfirm}
        setModalVisible={setModalVisibleConfirm}
        handleYes={handleDelete}
        handleCancel={handleCancel}
      />
    </View>
  );
}
