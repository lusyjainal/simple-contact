import React, {Component} from 'react';
import {ListItem, Avatar} from '@rneui/themed';
import TouchableScale from 'react-native-touchable-scale';
import {Icon} from '@rneui/themed';

const ListElement = ({id, fullName, age, photo, modulus = 1, handleDelete, handleEdit}) => {
  return (
    <ListItem
      Component={TouchableScale}
      friction={90} //
      tension={100} // These props are passed to the parent component (here TouchableScale)
      activeScale={0.95}
      containerStyle={{
        backgroundColor: modulus === 1 ? '#FF9800' : '#F44336',
        borderRadius: 10,
        marginBottom: 10,
      }}>
      <Avatar
        size={50}
        rounded
        source={{
          uri: photo,
        }}
      />
      <ListItem.Content>
        <ListItem.Title style={{color: 'white', fontWeight: 'bold'}}>
          {fullName}
        </ListItem.Title>
        <ListItem.Subtitle style={{color: 'white'}}>
          age {age}
        </ListItem.Subtitle>
      </ListItem.Content>
      <Icon 
        reverse 
        name="create" 
        type="ionicon" 
        color="blue" 
        size={20} 
        onPress={(contact)=>handleEdit(contact)}
      />
      <Icon 
        reverse 
        name="trash" 
        type="ionicon" 
        color="black" 
        size={20} 
        onPress={()=> handleDelete(id, fullName)}
      />
    </ListItem>
  );
};

export default ListElement;
