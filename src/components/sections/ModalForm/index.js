import ModalForm from './ModalForm';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        contact: state.contact
    }
}

export default connect(mapStateToProps)(ModalForm);