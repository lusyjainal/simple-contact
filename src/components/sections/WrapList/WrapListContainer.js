import React, {useEffect, useState} from 'react';
import {Alert, View, Text} from 'react-native';
import WrapList from './WrapList';
import {BASE_URL} from '../../../config/services';
import axios from 'axios';

/**
 * @description
 * file container di gunakan untuk kebutuhan logic
 */

export default function WrapListContainer({
    contactData,
    mainActions: {
        getContactDetail
    }
}) {
  const [contact, setContact] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [isLoadingForm, setIsLoadingForm] = useState(false);
  const [isLoadingConfirm, setIsLoadingConfirm] = useState(false);
  const [fullName, setFullName] = useState(false);
  const [modalVisibleConfirm, setModalVisibleConfirm] = useState(false);
  const [modalVisibleForm, setModalVisibleForm] = useState(false);
  const [id, setId] = useState('');

  const getData = async () => {
    setLoading(true);
    const req = await fetch(`${BASE_URL}/contact`);
    const {data} = await req.json();
    setLoading(false);
    setContact(data);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = () => {
    setIsLoadingConfirm(true)
    console.log(`${BASE_URL}/contact/${id}`);
    if (id) {
      axios
        .delete(`${BASE_URL}/contact/${id}`)
        .then(() => {
          setModalVisibleConfirm(false);
          getData();
          setIsLoadingConfirm(false);
        })
        .catch(err => {
          setModalVisibleConfirm(false);
          Alert.alert('Api Error 400');
          console.log(err);
          setIsLoadingConfirm(false);
        });
    }
  };

  const showModal = (id, fullName) => {
    setId(id)
    setFullName(fullName);
    setModalVisibleConfirm(true);
  };

  const showModalForm = (contact) => {
    getContactDetail(contact);
    setModalVisibleForm(true);
  }

  const handleSubmitData = values => {
    setIsLoadingForm(true);

    const {firstName, lastName, age, photo} = values;

    if(contactData.id){
        axios
          .put(`${BASE_URL}/contact/${contactData.id}`, {
            firstName,
            lastName,
            age: parseInt(age),
            photo,
          })
          .then(() => {
            setIsLoadingForm(false);
            getData();
            setModalVisibleForm(false);
            getContactDetail({})
          })
          .catch(err => {
            Alert.alert('Api Error 400');
            console.log(err);
            setIsLoadingForm(false);
            setModalVisibleForm(false);
            getContactDetail({})
          });
    } else {
        axios
          .post(`${BASE_URL}/contact`, {
            firstName,
            lastName,
            age: parseInt(age),
            photo,
          })
          .then(() => {
            setIsLoadingForm(false);
            getData();
            setModalVisibleForm(false);
          })
          .catch(err => {
            Alert.alert('Api Error 400');
            console.log(err);
            setIsLoadingForm(false);
            setModalVisibleForm(false);
          });
    }
  };

  const handleCancel = () => {
    setModalVisibleConfirm(false);
    setIsLoadingConfirm(false);
    setModalVisibleForm(false);
    setIsLoadingForm(false);
    getContactDetail({})
  }

  return (
    <View>
      <WrapList 
        contact={contact}
        isLoading={isLoading}
        isLoadingForm={isLoadingForm}
        isLoadingConfirm={isLoadingConfirm}
        fullName={fullName}
        modalVisibleConfirm={modalVisibleConfirm}
        setModalVisibleConfirm={setModalVisibleConfirm}
        modalVisibleForm={modalVisibleForm}
        setModalVisibleForm={setModalVisibleForm}
        handleSubmitData={handleSubmitData}
        showModal={showModal}
        showModalForm={showModalForm}
        handleCancel={handleCancel}
        handleDelete={handleDelete}
      />
    </View>
  );
}
