import { MODE_ENV } from '../constants/variable';
let BASE_URL; // dev

switch (MODE_ENV) {
    case 'production':
        BASE_URL = 'https://api.simple-contact-crud.herokuapp.com';
        break;
    case 'staging':
        BASE_URL = 'https://staging.simple-contact-crud.herokuapp.com';
        break;
    case 'development':
        BASE_URL = 'https://simple-contact-crud.herokuapp.com';
        break;
    default:
        BASE_URL = 'https://simple-contact-crud.herokuapp.com' // for local
}

export {
    BASE_URL
}
