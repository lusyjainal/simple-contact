/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import {WrapList} from './components/sections';
import {Provider} from 'react-redux';
import {store} from './redux';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { Home, SplashScreen } from './screens';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      {/* <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.wrapScroolView}>
          <WrapList />
        </ScrollView>
      </SafeAreaView> */}
      <NavigationContainer>
        <Stack.Navigator 
          initialRouteName="SplashScreen"
          screenOptions={{ headerShown: false }}
          >
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({
  wrapScroolView: {
    padding: 10,
  },
});

export default App;
