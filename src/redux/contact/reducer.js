import { ACTIONS } from "../../constants";

const contact = (state={}, action) => {
    switch (action.type) {
        case ACTIONS.GET_CONTACT_DETAIL:
            return action.contact
    
        default:
           return state
    }
}

export default contact;