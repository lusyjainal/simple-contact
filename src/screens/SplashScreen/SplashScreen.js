import {SafeAreaView, Image, Text, StyleSheet} from 'react-native';
import React, {useEffect} from 'react';
import { StackActions } from '@react-navigation/native';

export default function SplashScreen(props) {
  useEffect(() => {
    setTimeout(() => {
        props.navigation.dispatch(StackActions.replace('Home'))
    }, 3000);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Image 
        style={styles.img}
        source={require('../../assets/images/contact-us.png')}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#7CA1B4',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white'
    },
    img: {
        width: 200,
        height: 200
    }
})